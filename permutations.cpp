/*
Problem:
We are given S, a length n string of characters from the set {'D', 'I'}. (These letters stand for "decreasing" and "increasing".)

A valid permutation is a permutation P[0], P[1], ..., P[n] of integers {0, 1, ..., n}, such that for all i:

If S[i] == 'D', then P[i] > P[i+1], and;
If S[i] == 'I', then P[i] < P[i+1].
How many valid permutations are there?  Since the answer may be large, return your answer modulo 10^9 + 7.
*/
class Solution {
public:
    int numPermsDISequence(string S) {
        int n = S.size() + 1, d = 0, i = 0;
        vector<int> arr(n, 1);
        auto func = [](int a, int b) { return (a + b) % 1000000007; };
        for (char c : S) {
            if (c == 'D') {
                ++d;
                partial_sum(arr.rbegin() + i, arr.rend() - d, arr.rbegin() + i, func);
            } else {
                ++i;
                partial_sum(arr.begin() + d, arr.end() - i, arr.begin() + d, func);
            }
        }
        return *(arr.begin() + d);
    }
};